abstract public class Engine {

    protected String id;
    protected String companyName;
    protected String model;
    protected double volume;
    protected int yearOfProduction;

    public Engine() {}

    public Engine(final String id, final String companyName, final String model, double volume, int yearOfProduction){
        this.id = id;
        this.companyName = companyName;
        this.model = model;
        this.volume =volume;
        this.yearOfProduction = yearOfProduction;
    }

    abstract public void start();

    public void stop(){
        System.out.println("The engine has stopped.");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    @Override
    public String toString(){
        return '[' + id + '|' + companyName + '|' + model + '|' + volume + '|' + yearOfProduction + ']';
    }
}


abstract class BMWEngine extends Engine{

    public BMWEngine(String id, String model, double volume, int yearOfProduction){
        super(id, "BMW", model, volume, yearOfProduction);
    }

    public void stop(){
        System.out.println("The BMW engine has stopped.");
    }
}


abstract class BMWEngine1_6 extends BMWEngine{
    public BMWEngine1_6(String id, String model, int yearOfProduction) {
        super(id, model, 1.6, yearOfProduction);
    }
}

abstract class BMWEngine1_2 extends BMWEngine{
    public BMWEngine1_2(String id, String model, int yearOfProduction) {
        super(id, model, 1.2, yearOfProduction);
    }
}


class BMWEngineSuperModel1_6 extends BMWEngine1_6{
    public BMWEngineSuperModel1_6(String id, int yearOfProduction){
        super(id, "Super", yearOfProduction);
    }

    @Override
    public void start(){
        System.out.println("The BMW Super Model 1.6 has started.");
    }
}


class BMWEngineSuperModel1_2 extends BMWEngine1_2{
    public BMWEngineSuperModel1_2(String id, int yearOfProduction){
        super(id, "Super", yearOfProduction);
    }

    @Override
    public void start(){
        System.out.println("The BMW Super Model 1.2 has started.");
    }
}


class BMWEngineRegularModel1_2 extends BMWEngine1_2{
    public BMWEngineRegularModel1_2(String id, int yearOfProduction){
        super(id, "Regular", yearOfProduction);
    }

    @Override
    public void start(){
        System.out.println("The BMW Regular Model 1.2 has started.");
    }
}



















