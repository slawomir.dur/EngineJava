import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        System.out.println("Choose model of engine: [Super / Regular]");
        String engine;
        Scanner in = new Scanner(System.in);
        engine = in.next();

        while (!engine.equals("Super") && !engine.equals("Regular")){
            System.out.println("Insert correct model [Super / Regular]");
            engine = in.next();
        }

        Car car = new Car();
        if (engine.equals("Super")){
            car.setEngine(new BMWEngineSuperModel1_2("1234567", 2017));
        }
        else{
            car.setEngine(new BMWEngineRegularModel1_2("987654", 2008));
        }
        car.start();
        car.printEngineInformation();

    }

}
